// var menu = $('.navbar-collapse');
var header = null,
  headerAdmin = true,
  headerFillFlag = true,
  wpAdmin;

jQuery(document).ready(function($) {
  $(document).ready( e => {
    header = $('.elementor-location-header');
    wpAdmin = $(document).find('#wpadminbar');
      
    if (header.length !== 0 && wpAdmin.length !== 0 &&
      ((window.innerWidth > 600) || (window.innerWidth < 600 && window.pageYOffset < 32))
    ) {
      header.addClass('logged-admin');    
    }
  });
  
  
  $(window).on('scroll', e => {
  
    if (header.length !== 0) {
      if (e.currentTarget.pageYOffset > 32 ) {
    
        if (window.innerWidth < 600 && wpAdmin && wpAdmin.length !== 0) {
          header.removeClass('logged-admin');
        }
      }
      
      else {
        if (wpAdmin && wpAdmin.length !== 0 && window.innerWidth < 600) {
          header.addClass('logged-admin');
        }   
      }
    }
  });
})

