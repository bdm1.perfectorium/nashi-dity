// import Regions from "../regions";

let Regions = [
    {
        city: 'м.Київ',
        region: 'Національна соціальна сервісна служба України',
        address: 'вул.Еспланадна, 8/10, 01601',
        phones: [
            '(044) 289 87 89',
        ],
        coordinates: '50.43881,30.52121',
        text: 'для іноземців вхідна точка',
        time: ['Пн-Чт: 09:00-18:00', 'Пт: 09:00-16:45', 'Сб-Hд: вихідний']
    },
    {
        city: "м. Вінниця",
        region: 'Вінницька',
        address: 'вул. Монастирська, 47, 21050',
        phones: [
            '(0432)67-51-69',
            '67-14-08, 52-45-52',
            'факс 67-31-08'
        ],
        coordinates: '49.23092, 28.47038',
        time: ['Пн-Чт: 09:00-18:00', 'Пт: 09:00-17:00', 'Сб-Hд: вихідний']
    },
    {
        city: "м. Луцьк",
        region: 'Волинська',
        address: 'вул. Винниченка, 67, 43005',
        phones: [
            '(0332) 72-34-03',
            '24-60-61'
        ],
        coordinates: '50.75213, 25.32964'
    },
    {
        city: "м.Дніпро",
        region: 'Дніпропетровська',
        address: 'пр. Олександра Поля, 83/1, 49000',
        phones: [
            '(056) 732-48-74',
            '732-48-71-ф.',
            '(056) 732-48-76'
        ],
        coordinates: '48.45296661115384,35.02318125137324'    
    },
    {
        city: "м. Краматорськ",
        region: 'Донецька',
        address: 'пл. Миру, 284313',
        phones: [
            '(0626) 48-52-23',
        ],
        coordinates: '48.73764552256281,37.58562842762393'        
    },
    {
        city: "м. Житомир",
        region: 'Житомирська',
        address: 'вул. Мала Бердичівська, 25, 10014',
        phones: [
            '(0412) 47 50 11',
            '(0412) 42 20 79'
        ],
        coordinates: '50.25247718040214, 28.66494793316281',
        time: ['Пн-Чт: 08:00-17:15', 'Пт: 89:00-16:00', 'Сб-Hд: вихідний']        
    },
    {
        city: "м. Ужгород",
        region: 'Закарпатська',
        address: 'пл. Народна, 4, 88008',
        phones: [
            '(0312) 61-39-06'
        ],
        coordinates: '48.626639207014584, 22.288668046580437',
        time: ['Пн-Пт: 08:00-17:00','Сб-Hд: вихідний']        
    },
    {
        city: "м. Запоріжжя",
        region: 'Запорізька',
        address: 'пр. Соборний, 164, 69107',
        phones: [
            '(061) 233 11 91',
            '(061) 239 03 53'
        ],
        coordinates: '47.83965506455448,35.14052163003801',
        time: ['Пн-Пт: 08:00-17:00','Сб-Hд: вихідний']                
    },
    {
        city: "м.Івано-Франківськ",
        region: 'Івано-Франківська',
        address: 'вул.Грушевського, 21, 76004',
        phones: [
            '(0342)55-25-22-ф.'
        ],
        coordinates: '48.92252385713343, 24.71415149556854',
        time: ['Пн-Чт: 08:00-17:15', "Пт: 08:00-16:15",'Сб-Hд: вихідний']        
    },
    {
        city: "м. Київ",
        region: 'Київська',
        address: 'пл. Лесі Українки, 1, 01601',
        phones: [
            '286-85-07-ф',
            '284-83-26'
        ],
        coordinates: '50.428006716322955,30.54125726684109',
        time: ['Пн-Чт: 09:00-18:00', "Пт: 09:00-17:45",'Сб-Hд: вихідний']        
    },
    {
        city: 'м. Кропивницький',
        region: 'Кіровоградська',
        address: 'вул. Тараса Карпи, 84, кім. 110-112, 25006',
        phones: [
            '(052) 22-54-98',
            '22-34-96-ф.',
            '(052) 22-54-98-ф.'
        ],
        coordinates: '48.506795064713536,32.27048320245409',
        time: ['Пн-Чт: 08:00-17:00', "Пт: 08:00-15:45",'Сб-Hд: вихідний']        
    },
    {
        city: "м. Северодонецьк",
        region: 'Луганська',
        address: 'пр.Центральний, 26',
        phones: [
            '(06452) 4-24-20'
        ],
        coordinates: '48.949650691745916,38.491217649120244'        
    },
    {
        city: "м. Львів",
        region: 'Львівська',
        address: 'вул. Валова, 31, 79008',
        phones: [
            '(032) 235-44-56-ф.'
        ],
        coordinates: '49.840631427626, 24.03550592695927',
        time: ['Пн-Пт: 09:00-18:00','Сб-Hд: вихідний']        
    },
    {
        city: "м. Миколаїв",
        region: 'Миколаївська',
        address: 'вул. Наваринська, 16, 54001',
        phones: [
            '(0512) 47-38-32',
            '47-38-17-ф.',
            '(0512) 47-38-27'
        ],
        coordinates: '46.97180574558623,31.980985942867783',
        time: ['Пн-Чт: 08:00-17:00', "Пт: 08:00-16:00", 'Сб-Hд: вихідний']        
    },
    {
        city: "м. Одеса",
        region: 'Одеська',
        address: 'пр. Шевченка, 4, 65032',
        phones: [
            '(048) 728-37-95',
            '728-35-44'
        ],
        coordinates: '46.45792965009977,30.749266864375414',
        time: ['Пн-Чт: 08:00-17:00', "Пт: 08:00-15:45", 'Сб-Hд: вихідний']        
    },
    {
        city: "м. Полтава",
        region: 'Полтавська',
        address: 'вул. Соборна, 45, к.201, 36014',
        phones: [
            '(0352) 50-27-27-ф.',
            '56-97-93',
            '50-27-27-ф.'
        ],
        coordinates: '49.58287212964958,34.56595326182514'        
    },
    {
        city: "м. Рівне",
        region: 'Рівненська',
        address: 'майдан Просвіти, 1, 33013',
        phones: [
            '(0362) 69-51-99-ф.',
            '22-13-10'
        ],
        coordinates: '50.626060836054904, 26.254441977581592',
        time: ['Пн-Чт: 09:00-18:15', "Пт: 09:00-17:00", 'Сб-Hд: вихідний']        
    },
    {
        city: "м. Суми",
        region: 'Сумська',
        address: 'вул. Іллінська,97 26, 40009',
        phones: [
            '(0542)663585'
        ],
        coordinates: '50.91370421473988,34.77338567588966',
        time: ['Пн-Чт: 09:00-16:00', "Пт: 09:00-15:00", 'Сб-Hд: вихідний']        
    },
    {
        city: "м. Тернопіль",
        region: 'Тернопільська',
        address: 'вул. Грушевського 8, каб. 328, 46000',
        phones: [
            '(0352) 52-63-63',
            '52-21-81-ф.',
            '(0352) 52-21-81'
        ],
        coordinates: '49.55561239420833, 25.59181987599305',
        time: ['Пн-Чт: 08:00-17:15', "Пт: 08:00-16:00", 'Сб-Hд: вихідний']        
    },
    {
        city: "м. Харків",
        region: 'Харківська',
        address: 'Будинок Держпрому, 6 під’їзд, 4 поверх, к. 587,589, 61022',
        phones: [
            '757-44-36-ф.',
            '(057) 757-44-35'
        ],
        coordinates: '50.00676508851974,36.227075416028406',
        time: ['Пн-Hд: 06:00-22:00']        
    },
    {
        city: "м.Херсон",
        region: 'Херсонська',
        address: 'пл. Свободи, 1 , к.406, 407, 73000',
        phones: [
            '(0552) 49-20-79',
            'факс (0552) 22-64-87'
        ],
        coordinates: '46.64092903105581,32.61454728575557',
        time: ['Пн-Пт: 08:00-17:00', 'Сб-Hд: вихідний']        
    },
    {
        city: "м.Херсон",
        region: 'Хмельницька',
        address: 'пл. Свободи, 1 , к.406, 407, 73000',
        phones: [
            '(0382) 65-80-71',
            '79-41-43-ф.',
            '65-61-62'
        ],
        coordinates: '49.420679166116344, 26.979412986547107',
        time: ['Пн-Чт: 08:00-17:15', 'Пт: 08:00-16:00', 'Сб-Hд: вихідний']        
    },
    {
        city: 'м. Черкаси',
        region: 'Черкаська',
        address: 'бул. Шевченка, 185, 18000',
        phones: [
            '(0472) 33-01-15-ф.',
            '(0472) 32-11-40'
        ],
        coordinates: '49.44510476361639,32.06067601787238',
        time: ['Пн-Чт: 08:00-17:00', 'Пт: 08:00-15:45', 'Сб-Hд: вихідний']        
    },
    {
        city: 'м. Чернігів',
        region: 'Чернігівська',
        address: 'проспект Миру, 19,14000',
        phones: [
            '(0462) 669-465',
            '651-764-ф.',
            '(0462) 651-764'
        ],
        coordinates: '51.490129268191346,31.30148476640867'        
    },
    {
        city: 'м.Київ',
        region: 'м. Київ',
        address: 'вул.Дегтярівська, 3а, 04050',
        phones: [
            '484-05-24',
            '484-05-25-ф.',
            '484-05-23'
        ],
        coordinates: '50.46104590637108,30.48257246027241'        
    },
];


jQuery(document).ready(function($) {

        let map = L.map('map').setView(['50.46104590637108','30.48257246027241'], 7);
        let popup = $('.map-popup'),
            content = popup.find('.map-popup-content'),
            close = popup.find('.map-popup-close'),
            userLocation = null,
            routingContext = null;
        
        L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=sk.eyJ1IjoiYWxleGtvcyIsImEiOiJjbDN5OXl3NXQwZ283M2pqajc5ZTNsYTUzIn0.0eW5-MRzigQF_cs9lz1F9A', {
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            maxZoom: 18,
            id: 'mapbox/streets-v11',
            tileSize: 512,
            zoomOffset: -1,
            accessToken: 'sk.eyJ1IjoiYWxleGtvcyIsImEiOiJjbDN5OXl3NXQwZ283M2pqajc5ZTNsYTUzIn0.0eW5-MRzigQF_cs9lz1F9A'
        }).addTo(map);
        
        L.control.locate().addTo(map);
        
        map.locate()
        .on('locationfound', (e) => {
            userLocation = [e.latlng.lat, e.latlng.lng];
            L.marker(userLocation).addTo(map);
        }).on('locationerror', (e) => {
            navigator.geolocation.getCurrentPosition((success) => {
                console.log(success);
            }, (err) => {
                console.log(err)
            })
        });
        
        var icon = L.icon({
            iconUrl: (window.templateUrl ?? '') + '/images/marker.svg',
            iconSize: [49, 68],
        });
        
        Regions.forEach(item => {
            if (item.coordinates.length) {
                L.marker(item.coordinates.split(','), {
                    icon: icon
                }).addTo(map).addEventListener('click', () => showPopup(item))
            }
        });
        
        const showPopup = (props) => {
            content.empty();
            popup.removeClass('hidden');
        
            if (props.region) {
                content.append(`<h3 class="region">Служба у справах дітей </br> ${props.city}</h3>`);
            }
        
            content.append(`
                <ul class="map-popup-content-info">
                ${
                    props.address ? `
                        <li class="map-popup-content-info-item">    
                            <img class="map-popup-content-info-item-icon" src="${window.templateUrl ?? ''}/images/address.svg"/>
                            
                            <div class="map-popup-content-info-item-content">
                                <span>${props.address}</span>                        
                                <span class="go-to" data-location="${props.coordinates}">ПРОКЛАСТИ МАРШРУТ</span>                
                            </div>
                        <li>
                    ` : ''
                }
                ${
                    props.phones ? `
                        <li class="map-popup-content-info-item">    
                            <img class="map-popup-content-info-item-icon" src="${window.templateUrl ?? ''}/images/phone.svg"/>
                            
                            <div class="map-popup-content-info-item-content">
                                ${props.phones.map(item => `<a class="phone" href="tel:${item}">${item}</a>`).join("")}
                            </div>
                        <li>
                    ` : ''
                }
        
                ${
                    props.time ? `
                        <li class="map-popup-content-info-item">    
                            <img class="map-popup-content-info-item-icon" src="${window.templateUrl ?? ''}/images/time.svg"/>
                            
                            <div class="map-popup-content-info-item-content">
                                ${props.time.map(item => `<span>${item}</span>`).join("")}
                            </div>
                        <li>
                    ` : ''
                }
                <ul>
            
            `)
        
            let getRoute = content.find('.go-to');
            
            if (getRoute.length !== 0) {
        
                getRoute.on('click', () => {
        
                    if (routingContext) {
                        map.removeControl(routingContext);
                    }
                    routingContext = L.Routing.control({
                        waypoints: [
                            L.latLng(userLocation[0], userLocation[1]),
                            L.latLng(getRoute.attr('data-location').split(','))
                        ],
                        createMarker: function() { return null; }
                    }).addTo(map);
                })
            }
        }
        
        
        close.on('click', () => {
            popup.addClass('hidden');
            content.empty();
        
            if (routingContext) {
                map.removeControl(routingContext);
                routingContext = null;
            }
        });

});
