// Plugins
// import 'jquery';
// import 'js-polyfills';
// import "leaflet";
// import "leaflet-routing-machine";
// import "leaflet.locatecontrol";

import "../styles/app.scss";

// Components
import './components/component-header.js';
import './components/component-map.js';